extends NinePatchRect

onready var ip_label = $VBoxContainer/ServerInfoContainer/ServerSettings/VBoxContainer/ScrollContainer/IPOutput
onready var port_label = $VBoxContainer/ServerInfoContainer/ServerSettings/VBoxContainer/HBoxContainer/PortOutput
onready var player_list = $VBoxContainer/PlayerContainer/VBoxContainer/PlayerList

var map_resource = "res://Maps/Map002/World002.tscn"

func _ready():
	Multiplayer.connect("reload_gui", self, "set_player_list")
	Network.connect("reload_gui", self, "set_gui")
	
	if get_tree().is_network_server():
		server_set_ips()
	
	set_gui()
	set_player_list()

func server_set_ips():
	Network.server_set_ips(Network.public_ip_lookup)

func set_gui():
	ip_label.text = String(Network.formatted_server_ips)
	port_label.text = String(Network.port)

func set_player_list():
	player_list.clear()
	
	for player in Multiplayer.player_info:
		player_list.push_color(Color(Multiplayer.player_info[player].color))
		player_list.append_bbcode(String(Multiplayer.player_info[player].username) + "\n")

func _on_ToolButton_pressed():
	OS.set_clipboard(Network.server_ips[0] + ":" + String(Network.port))

func _on_StartGame_pressed():
	if Network.is_server and Multiplayer.player_info.size() > 1:
		rpc("start_level", map_resource)

remotesync func start_level(map):
	# Players can't connect to the server anymore
	get_tree().set_refuse_new_network_connections(true)
	Loader.load_map(map)


func _on_BTTMMButton_pressed():
	Multiplayer.stop()
