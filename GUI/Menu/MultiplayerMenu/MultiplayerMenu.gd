extends NinePatchRect

func _on_HostGameButton_pressed():
	start_game(true, $VBoxContainer/HostGameContainer/HostPort)
	
func _on_JoinGameButton_pressed():
	Network.set_server_ips([$VBoxContainer/JoinGameContainer/JoinIP.text])
	start_game(false, $VBoxContainer/JoinGameContainer/JoinPort)

func _hide_outputLabel():
	$VBoxContainer/OutputLabel.hide()

func valid_port(port):
	return port > 0 and port < 65535

func start_game(is_server, port_line_text_field):
	var port = int(port_line_text_field.text)
	
	if valid_port(port):
		Network.port = port
		Network.is_server = is_server
		get_tree().change_scene("GUI/Menu/MultiplayerLoadingScreen/MultiplayerLoadingScreen.tscn")
	else:
		show_error("Invalid Port")
		port_line_text_field.clear()

func show_error(s):
	$VBoxContainer/OutputLabel.show()
	$VBoxContainer/OutputLabel.text = s
	
	var timer = Timer.new()
	timer.connect("timeout",self,"_hide_outputLabel") 
	# timeout is what says in docs, in signals
	# self is who respond to the callback
	# _on_timer_timeout is the callback, can have any name
	add_child(timer) # to process
	timer.start(5) # to start
