extends NinePatchRect

onready var name_lineedit = $VBoxContainer/NameContainer/NameLineEdit
onready var color_rect = $VBoxContainer/ColorContainer/ColorChangeContainer/ColorRect

onready var color_picker = $ColorPickerPopup/ColorPicker

func _ready():
	name_lineedit.text = Player.username
	color_rect.color = Player.color


func _on_ColorChangeButton_pressed():
	$ColorPickerPopup.popup_centered_ratio(.4)

func _on_ColorPickerPopup_confirmed():
	Player.color = color_picker.color
	color_rect.color = Player.color

func _on_NameLineEdit_text_changed(new_text):
	Player.username = new_text


func _on_BTTMMButton_pressed():
	Multiplayer.my_info = { username = Player.username, color = Player.color }
