extends MarginContainer

func _ready():
	Multiplayer.connect("connected_to_server", self, "_load_lobby")
	Multiplayer.start()
	
	if Network.is_server:
		_load_lobby()

func _load_lobby():
	get_tree().change_scene("GUI/Menu/MultiplayerLobby/MultiplayerLobby.tscn")
