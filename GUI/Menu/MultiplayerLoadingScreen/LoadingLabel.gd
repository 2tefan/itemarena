extends Label

export var max_points = 5
export var time = .2

var current_points = 0

var timer

func _ready():
	timer = Timer.new()
	timer.connect("timeout", self, "continue_loading")
	add_child(timer) # to process
	timer.start(time) # to start


func continue_loading():
	#TranslationServer.set_locale("en")

	if current_points < max_points:
		current_points += 1
	else:
		current_points = 0

	var points = ""

	for i in current_points:
		points += "."

	text = tr("LABEL_CONNECTING_TO_SERVER") + "\n" + points
	
	timer.start(time)