extends CenterContainer

var loader
var wait_frames
var time_max = 100 # msec
var current_scene
var root

onready var progressbar = $VBoxContainer/ProgressBar

func _ready():
	root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() -1)
	
	goto_scene(Global.current_map)

func _process(time):
	if loader == null:
		# no need to process anymore
		set_process(false)
		return

	if wait_frames > 0: # wait for frames to let the "loading" animation show up
		wait_frames -= 1
		return

	var t = OS.get_ticks_msec()
	while t < t + time_max: # use "time_max" to control for how long we block this thread

		# poll your loader
		var err = loader.poll()

		if err == ERR_FILE_EOF: # Finished loading.
			var resource = loader.get_resource()
			loader = null
			set_new_scene(resource)
			break
		elif err == OK:
			update_progress()
		else: # error during loading
			Multiplayer.return_to_multiplayer_menu("Error on map load!")
			loader = null
			break

func goto_scene(path): # game requests to switch to this scene
	if path == null:
		Multiplayer.return_to_multiplayer_menu("Nothing to load!")
		return
	
	loader = ResourceLoader.load_interactive(path)
	if loader == null: # check for errors
		Multiplayer.return_to_multiplayer_menu("Error on map load!")
		return
	set_process(true)
	
	wait_frames = 1

func update_progress():
	var progress = float(loader.get_stage()) / loader.get_stage_count()
	# Update your progress bar?
	progressbar.value = progress


func set_new_scene(scene_resource):
	root.add_child(scene_resource.instance())
	
	get_parent().remove_child(self)
	self.call_deferred("free")
