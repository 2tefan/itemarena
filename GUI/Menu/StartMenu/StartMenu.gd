extends MarginContainer

func _on_StartSingleplayer_pressed():
	get_tree().change_scene("Maps/Map001/World001.tscn")


func _on_StartMultiplayer_pressed():
	get_tree().change_scene("GUI/Menu/MultiplayerMenu/MultiplayerMenu.tscn")


func _on_QuitGame_pressed():
	get_tree().quit()


func _on_PlayerSettings_pressed():
	get_tree().change_scene("res://GUI/Menu/PlayerSettings/PlayerSettings.tscn")


func _on_TestLoad_pressed():
	Loader.load_game_results(3)
