extends RichTextLabel

func append_headline(headline):
	append_bbcode("[b]" + headline + "[/b]\n")

func append_players(player_array):
	# [player_id, value], ...
	
	for element in player_array:
		push_color(Multiplayer.get_color(int(element[0])))
		append_bbcode(String(Multiplayer.get_username(int(element[0]))))
		push_color(Color.white)
		
		if Global.debug_mode:
			append_bbcode(" (" + String(element[0]) + ")")
		
		append_bbcode(" [" + String(element[1]) + "]\n")
