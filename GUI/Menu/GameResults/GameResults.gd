extends VBoxContainer

# results = { category = "Test category", array = [[player_id, value], [...]]  }
# var test_results = { category = "Test category", array = [[1, 5], [3, 3], [194312, 2]]}

onready var results_rtl = $VBoxContainer/ResultsRTL

func set_results(results):
	results_rtl.clear()
	
	for element in results:
		results_rtl.append_headline(element.category)
		results_rtl.append_players(element.array)


func _on_BTTMMButton_pressed():
	Multiplayer.stop()
