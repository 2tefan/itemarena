extends KinematicBody2D

export (int) var speed = 200
export (int) var strength = 250
export (int) var spin_strength = 50

var velocity = Vector2()

var item1
var item2
var item3
var item4

func get_input():
	velocity = Vector2()
	if Input.is_action_pressed("game_move_right"):
		velocity.x += 1
	if Input.is_action_pressed("game_move_left"):
		velocity.x -= 1
	if Input.is_action_pressed("game_move_up"):
		velocity.y -= 1
	if Input.is_action_pressed("game_move_down"):
		velocity.y += 1
	velocity = velocity.normalized() * speed

# Rotates the player, depending where the mouse curse is located
func get_rotation():
	$CollisionPolygon2D.rotation = get_global_mouse_position().angle_to_point(position)

func get_items():
	if $PickupArea.get_overlapping_bodies():
		if Input.is_action_just_pressed("game_item_1"):
			item1 = get_item($Items/PlayerItem1)
		if Input.is_action_just_pressed("game_item_2"):
			item2 = get_item($Items/PlayerItem2)
		if Input.is_action_just_pressed("game_item_3"):
			item3 = get_item($Items/PlayerItem3)
		if Input.is_action_just_pressed("game_item_4"):
			item4 = get_item($Items/PlayerItem4)

func get_item(PlayerItem):
	var item = null
	
	for i in $PickupArea.get_overlapping_bodies(): # Looks for all overlapping items
		if i.get_parent() != null: # If it has a parent, then it's still active -> player can pickup that
			item = i
			
			PlayerItem.replaceByNewItem(item) # Replaces the item the player carries
			item.get_parent().remove_child(item)
			item.set_angular_velocity(0) # Removes angluar velocity
			item.set_linear_velocity(Vector2(0,0)) # Removes velocity
			item.rotation = 0
			break
	
	return item

func release_items():
	if Input.is_action_just_released("game_item_1"):
		item1 = release_item($Items/PlayerItem1, item1)
	if Input.is_action_just_released("game_item_2"):
		item2 = release_item($Items/PlayerItem2, item2)
	if Input.is_action_just_released("game_item_3"):
		item3 = release_item($Items/PlayerItem3, item3)
	if Input.is_action_just_released("game_item_4"):
		item4 = release_item($Items/PlayerItem4, item4)

func release_item(PlayerItem, item):
	if(item != null):
		PlayerItem.restore_player_item() # Removes item from "inventory"
		$"../Items".add_child(item) # Adds the item back to the game
		item.position = global_position
		item.set_linear_velocity(Vector2(cos($CollisionPolygon2D.rotation),sin($CollisionPolygon2D.rotation))*strength)
		#item.set_angular_velocity(spin_strength)
		return null # Return null to remove item from variable

func _physics_process(delta):
	get_input()
	get_rotation()
	get_items()
	release_items()
	velocity = move_and_slide(velocity)
