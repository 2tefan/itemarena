extends KinematicBody2D

export (int) var speed = 2000
export (int) var strength = 2500
export (int) var spin_strength = 50

onready var map = get_parent().get_parent()

var velocity = Vector2()

onready var items = [$Items/PlayerItem1, $Items/PlayerItem2, $Items/PlayerItem3, $Items/PlayerItem4]

var default_player_item_scene = preload("res://Items/PlayerItem/PlayerItem.tscn")

puppet var puppet_pos = Vector2()
puppet var puppet_velocity = Vector2()
puppet var puppet_rotation = 0

puppet var puppet_item_types = [Items.ITEM_TYPES.NOTHING, Items.ITEM_TYPES.NOTHING, Items.ITEM_TYPES.NOTHING, Items.ITEM_TYPES.NOTHING]

func get_input():
	velocity = Vector2()
	if Input.is_action_pressed("game_move_right"):
		velocity.x += 1
	if Input.is_action_pressed("game_move_left"):
		velocity.x -= 1
	if Input.is_action_pressed("game_move_up"):
		velocity.y -= 1
	if Input.is_action_pressed("game_move_down"):
		velocity.y += 1
	velocity = velocity.normalized() * speed
	
	if velocity.length() != 0:
		rset_unreliable("puppet_pos", position)
		rset_unreliable("puppet_velocity", velocity)

# Rotates the player, depending where the mouse curse is located
func get_rotation():
	if $CollisionPolygon2D.rotation != get_global_mouse_position().angle_to_point(position):
		$CollisionPolygon2D.rotation = get_global_mouse_position().angle_to_point(position)
		rset_unreliable("puppet_rotation", $CollisionPolygon2D.rotation)
		rset_unreliable("puppet_pos", position)

func get_items():
	if $PickupArea.get_overlapping_bodies(): # Is only executed if there are any items nearby
		if Input.is_action_just_pressed("game_item_1"):
			get_item(0)
		if Input.is_action_just_pressed("game_item_2"):
			get_item(1)
		if Input.is_action_just_pressed("game_item_3"):
			get_item(2)
		if Input.is_action_just_pressed("game_item_4"):
			get_item(3)

func get_item(number):
	var player_item = items[number]
	
	for i in $PickupArea.get_overlapping_bodies(): # Looks for all overlapping items
		if i.get_parent() != map.hidden_items: # If parent is not node "hidden_items" -> Player is allowed to pickup the item
			var item = i
			player_item.rpc("set_linked_item", item.get_path())
			print(item.item_type)
			Items.rpc("remove_item_from_map", map.get_path(), player_item.linked_item.get_path())
			break
		else:
			print("Item not available anymore!") # If a other player / other item slot got the item before
	
	refresh_puppet_item(number)

func refresh_puppet_item(number):
	if items[number].linked_item != null and items[number].linked_item.item_type != puppet_item_types[number]:
		puppet_item_types[number] = items[number].linked_item.item_type
		rset("puppet_item_types", puppet_item_types)

func release_items():
	if Input.is_action_just_released("game_item_1"):
		release_item(0)
	if Input.is_action_just_released("game_item_2"):
		release_item(1)
	if Input.is_action_just_released("game_item_3"):
		release_item(2)
	if Input.is_action_just_released("game_item_4"):
		release_item(3)

func release_item(number):
	var item = items[number]
	
	if(item.linked_item != null):
		print("Release item!")
		Items.rpc("add_item_to_map", map.get_path(), item.linked_item.get_path(), global_position, Vector2(cos($CollisionPolygon2D.rotation), sin($CollisionPolygon2D.rotation))*strength)
		rpc("remove_item_from_inventory", number)

remotesync func remove_item_from_inventory(number):
	var item = items[number]
	item.restore_player_item() # Removes item from "inventory"
	puppet_item_types[number] = Items.ITEM_TYPES.NOTHING

func _physics_process(delta):
	if is_network_master():
		get_input()
		get_rotation()
		get_items()
		release_items()
	else:
		position = puppet_pos
		velocity = puppet_velocity
		$CollisionPolygon2D.rotation = puppet_rotation
		
		for i in range(puppet_item_types.size()):
			if(items[i].linked_item != null and items[i].linked_item.item_type != puppet_item_types[i]):
				items[i].change_sprite(puppet_item_types[i])
	
	velocity = move_and_slide(velocity)
