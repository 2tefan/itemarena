extends RigidBody2D

export var item_type = Items.ITEM_TYPES.NOTHING

onready var collision_shape = $CollisionShape2D

puppet var puppet_pos = position
puppet var puppet_angular_vel = angular_velocity
puppet var puppet_linear_vel = linear_velocity

func get_sprite():
	return $Sprite
