extends "res://Items/BaseItem/BaseItem.gd"

var linked_item = null

func restore_player_item():
	linked_item = null
	change_sprite(Items.ITEM_TYPES.NOTHING)

remotesync func set_linked_item(new_linked_item_path):
	var new_linked_item = get_node(String(new_linked_item_path))
	linked_item = new_linked_item
	change_sprite(linked_item.item_type)

func change_sprite(item_type):
	var temp_item_template = load("res://Items/" + Items.sprites[item_type] + "/" + Items.sprites[item_type] + ".tscn")
	var temp_item = temp_item_template.instance()
	self.get_sprite().set_texture(temp_item.get_sprite().get_texture())
	#item_type = new_item.item_type
