extends "res://Items/BaseItem/BaseItem.gd"

func _ready():
	move_event() # Sync pos/vel on _ready

func is_active() -> bool:
	print(sleeping)
	return !sleeping

func _on_NormalItem_body_entered(body):
	if Player.is_player(body):
		print(body.name)

func move_event():
	if is_network_master():
		rset("puppet_pos", position)
		rset("puppet_angular_vel", angular_velocity)
		rset("puppet_linear_vel", linear_velocity)
		rpc("set_puppet")
		#print("Server!")

remotesync func set_puppet():
	if !is_network_master():
		position = puppet_pos
		angular_velocity = puppet_angular_vel
		linear_velocity = puppet_linear_vel
		#print("Puppet!")

func _physics_process(delta):
	if is_network_master():
		if !is_sleeping():
			rset_unreliable("puppet_pos", position)
			rset_unreliable("puppet_angular_vel", angular_velocity)
			rset_unreliable("puppet_linear_vel", linear_velocity)
	else:
		set_puppet()

func disable_item():
	able_item(false)

func enable_item():
	able_item(true)

func able_item(enable):
	collision_shape.set_disabled(!enable)
	set_process(enable)
	set_physics_process(enable)
	set_process_input(enable)
