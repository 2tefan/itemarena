extends Area2D

var item_count = 0
var items = []
puppet var puppet_item_count = 0
puppet var puppet_items = []

var timer
onready var map = get_parent().get_parent()

func _ready():
	refresh_count()
	
	if is_network_master():
		timer = Timer.new()
		timer.connect("timeout", self, "_look_for_items") 
		# timeout is what says in docs, in signals
		# self is who respond to the callback
		# _on_timer_timeout is the callback, can have any name
		add_child(timer) # to process
		timer.start(1) # to start

func _look_for_items():
	for player in get_overlapping_bodies(): # Get every body in Area2D
		if Player.is_player(player): # Check if it's a player
			if name == player.name: # Check if the player and the playerbase have the same name
				for i in range(player.items.size()): # Go through the players items
					if _is_valid_item(player.items[i].linked_item): # If it's a valid item remove it from player and add it to base
						add_item(player.items[i].linked_item)
						player.rpc("remove_item_from_inventory", i)

func add_item(item):
	if is_network_master():
		item_count += 1
		items.append(item.name)
		rset("puppet_item_count", item_count)
		rset("puppet_items", items)
		rpc("refresh_count")

func remove_item(item):
	item_count -= 1
	items.erase(item.name)
	refresh_count()

func _is_valid_item(object): # Checks if it is an item and that it isn't a PlayerItem
	return object != null and "item_type" in object and object.item_type != Items.ITEM_TYPES.NOTHING

remotesync func refresh_count():
	if !is_network_master():
		item_count = puppet_item_count
		items = puppet_items
	
	$Label.text = String(item_count)
