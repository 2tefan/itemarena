extends Node2D

const PLAYER_TEMPLATE = preload("res://Player/MultiPlayer/MultiPlayer.tscn")
onready var hidden_items = $HiddenItems
onready var normal_items = $Items

func _ready():
	pre_configure_game()

var players_done = []
remote func done_preconfiguring(who):
	# Here are some checks you can do, for example
	assert(get_tree().is_network_server())
	assert(who in Multiplayer.player_info) # Exists
	assert(not who in players_done) # Was not added yet
	players_done.append(who)
	
	if players_done.size() == Multiplayer.player_info.size():
		rpc("post_configure_game")

remotesync func post_configure_game():
	get_tree().set_pause(false)
	# Game starts now!
	print("Start game!")

remote func pre_configure_game():
	
	var selfPeerID = get_tree().get_network_unique_id()

	# Load my player
	var my_player = PLAYER_TEMPLATE.instance()
	my_player.set_name(str(selfPeerID))
	my_player.set_network_master(selfPeerID) # Will be explained later
	get_node("Players").add_child(my_player)

	# Load other players
	for p in Multiplayer.player_info:
		var player = PLAYER_TEMPLATE.instance()
		player.set_name(str(p))
		player.set_network_master(p) # Will be explained later
		get_node("Players").add_child(player)
	
	if !Network.isServer:
		# Tell server (remember, server is always ID=1) that this peer is done pre-configuring.
		rpc_id(1, "done_preconfiguring", selfPeerID)
		
	get_tree().set_pause(true) # Pre-pause