extends Node2D

export var map_width = 1024
export var map_height = 1024

var _pre_pos_x = (map_width / 2) - (Map.player_base_size / 2) - (Map.border_width / 2)
var _pre_pos_y = (map_height / 2) - (Map.player_base_size / 2) - (Map.border_width / 2)

var default_playerbase_pos = [Vector2(_pre_pos_x, _pre_pos_y), Vector2(_pre_pos_x, -_pre_pos_y), Vector2(-_pre_pos_x, -_pre_pos_y), Vector2(-_pre_pos_x, _pre_pos_y)]
export var playerbase_pos := PoolVector2Array()

var players_done = []

const PLAYER_TEMPLATE = preload("res://Player/MultiPlayer/MultiPlayer.tscn")
const PLAYERBASE_TEMPLATE = preload("res://MapElements/PlayerBase/PlayerBase.tscn")
const NORMAL_ITEM_TEMPLATE = preload("res://Items/NormalItem/NormalItem.tscn")
const SPAWN_BUFFER_TEMPLATE = preload("res://Items/SpawnBuffer/SpawnBuffer.tscn")

onready var hidden_items = $HiddenItems
onready var normal_items = $Items
onready var players = $Players
onready var player_bases = $Bases
var count_items = 0

var playtime_timer
export var playtime: int  = 60 # In secounds

var item_spawn_timer
export var item_spawn_time: float = 5

signal free_position

func _ready():
	Multiplayer.connect("remove_player", self, "_remove_player")
	pre_configure_game()

func _remove_player(id):
	var player_to_remove = players.get_node(String(id))
	player_to_remove.free()
	players_done.erase(id)

remote func done_preconfiguring(who):
	# Checks
	assert(get_tree().is_network_server())
	assert(who in Multiplayer.player_info) # Exists
	assert(not who in players_done) # Was not added yet
	players_done.append(who)
	
	if players_done.size() == Multiplayer.player_info.size():
		rpc("post_configure_game")

remotesync func post_configure_game():
	get_tree().set_pause(false)
	# Game starts now!
	print("Start game!")

func server_end_game():
	playtime_timer.stop()
	playtime_timer = null
	item_spawn_timer.stop()
	item_spawn_timer = null
	call_deferred("_deferred_server_end_game")

func _deferred_server_end_game():
	# results = { category = "Test category", array = [[player_id, value], [...]]  }
	
	get_tree().set_pause(true)
	var results = []
	
	var result_most_items_array = []
	
	for i in players.get_child_count():
		result_most_items_array.append([player_bases.get_child(i).name, player_bases.get_child(i).item_count])
	
	var result_most_items = { category = "Most items", array = result_most_items_array}
	
	results.append(result_most_items)
	
	rpc("end_game", results)
	get_tree().set_pause(false)

remotesync func end_game(results):
	Loader.load_game_results(results)

remote func pre_configure_game():
	
	var selfPeerID = get_tree().get_network_unique_id()
	
	if playerbase_pos.size() < 2 or playerbase_pos.size() > 4:
		playerbase_pos = default_playerbase_pos
	
	# Load players
	var count_players = 0
	count_items = normal_items.get_child_count()
	
	var player_info_ids = Multiplayer.player_info.keys() # Dictonary IDs to Array
	player_info_ids.sort() # Sort Array (Because Dictonaries can't be sorted)
	
	for p in player_info_ids:
		var player = PLAYER_TEMPLATE.instance()
		player.set_name(str(p))
		player.global_position = playerbase_pos[count_players] # Set spawnpoint
		player.set_network_master(p) # Make player his own network master
		players.add_child(player) # Add player to the map
		
		var player_base = PLAYERBASE_TEMPLATE.instance()
		player_base.set_name(str(p))
		player_base.global_position = playerbase_pos[count_players]
		player_bases.add_child(player_base)
		count_players += 1
	
	if !Network.is_server:
		# Tell server (remember, server is always ID=1) that this peer is done pre-configuring.
		rpc_id(1, "done_preconfiguring", selfPeerID)
	else:
		# The server needs to append himself too
		players_done.append(selfPeerID)
		
		playtime_timer = Timer.new()
		playtime_timer.connect("timeout", self, "server_end_game") 
		add_child(playtime_timer) # to process
		playtime_timer.start(playtime)
		
		item_spawn_timer = Timer.new()
		item_spawn_timer.connect("timeout", self, "spawn_item_random") 
		add_child(item_spawn_timer) # to process
		item_spawn_timer.start(item_spawn_time)
		
	get_tree().set_pause(true) # Pre-pause

remotesync func spawn_item(pos : Vector2, count_items_ : int):
	count_items = count_items_
	count_items += 1
	var item = NORMAL_ITEM_TEMPLATE.instance()
	
	item.position = pos
	item.name = "NormalItem" + String(count_items)
	
	normal_items.add_child(item)

func spawn_item_random():
	get_free_position()
	var pos = yield(self, "free_position")
	if pos != null:
		rpc("spawn_item", pos, count_items)

func get_free_position(xBegin : int = -map_width / 2, xEnd : int = map_width / 2, yBegin : int = -map_height / 2, yEnd : int = map_height / 2):
	var max_spawn_tries = 100
	var spawn_buffer = SPAWN_BUFFER_TEMPLATE.instance()
	normal_items.add_child(spawn_buffer)
	spawn_buffer.position = Vector2(0,0)
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	
	var count_runs = 0
	
	while ((spawn_buffer.get_overlapping_bodies().size() + spawn_buffer.get_overlapping_areas().size()) > 0 and count_runs < max_spawn_tries) or count_runs == 0:
		spawn_buffer.position = Vector2(rng.randf_range(xBegin, xEnd), rng.randf_range(yBegin, yEnd))
		count_runs += 1
		yield(get_tree(), "idle_frame")
	
	if count_runs < max_spawn_tries:
		emit_signal("free_position", spawn_buffer.position)
	else:
		print("Tried " + String(count_runs) + " times to find a spawn location!")
		emit_signal("free_position", null)
	
	spawn_buffer.call_deferred("free")
	rng.call_deferred("free")
