extends Node

# Player info, associate ID to data
var player_info = {}
# Info we send to other players
var my_info = { username = Player.username, color = Player.color }

var peer

signal reload_gui
signal remove_player
signal connected_to_server

func start():
	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_ok")
	get_tree().connect("connection_failed", self, "_connected_fail")
	get_tree().connect("server_disconnected", self, "_server_disconnected")
	
	peer = NetworkedMultiplayerENet.new()
	if Network.is_server:
		create_server()
	else:
		create_client()
	
	get_tree().set_network_peer(peer)
	
	Multiplayer.player_info[get_tree().get_network_unique_id()] = my_info

func create_server():
	peer.create_server(Network.port, Network.players)

func create_client():
	peer.create_client(Network.server_ips[0], Network.port)

func stop():
	get_tree().set_network_peer(null) # Kills the networking feature
	get_tree().disconnect("network_peer_connected", self, "_player_connected")
	get_tree().disconnect("network_peer_disconnected", self, "_player_disconnected")
	get_tree().disconnect("connected_to_server", self, "_connected_ok")
	get_tree().disconnect("connection_failed", self, "_connected_fail")
	get_tree().disconnect("server_disconnected", self, "_server_disconnected")
	peer = null
	player_info = {}


############# Handel events #############
func _player_connected(id):
	# Called on both clients and server when a peer connects
	rpc_id(id, "register_player", Multiplayer.my_info) # Sends my info to new player
	print("Player connected!")

func _player_disconnected(id):
	# Player disconnected
	Multiplayer.player_info.erase(id) # Erase player from info.
	print("Player disconnected!")
	
	# Remove player from game
	emit_signal("remove_player", id)
	
	# Reload GUI
	emit_signal("reload_gui")

func _connected_ok():
	# Client sucessfully connected to server
	print("Sucessfully connected!")
	
	emit_signal("connected_to_server")

func _server_disconnected():
	# Server kicked client / Server shutdown
	print("Kicked by server!")
	call_deferred("stop")
	return_to_multiplayer_menu("Kicked by server!")

func _connected_fail():
	# Client can't connect
	print("Could not connect to server!")
	stop()
	return_to_multiplayer_menu("Could not connect to server!")

func return_to_multiplayer_menu(error_msg):
	call_deferred("_deferred_return_to_multiplayer_menu", "res://GUI/Menu/MultiplayerMenu/MultiplayerMenu.tscn", error_msg)

remote func register_player(info):
	# Get the id of the RPC sender.
	var id = get_tree().get_rpc_sender_id()
	# Store the info
	Multiplayer.player_info[id] = info
	
	# Reload GUI
	emit_signal("reload_gui")

func _deferred_return_to_multiplayer_menu(path, error_msg):
	var root = get_tree().get_root()
	var current_scene = root.get_child(root.get_child_count() - 1)
	
	# It is now safe to remove the current scene
	current_scene.free()

	# Load the new scene.
	var s = ResourceLoader.load(path)
	
	# Instance the new scene.
	current_scene = s.instance()

	# Add it to the active scene, as child of root.
	get_tree().get_root().add_child(current_scene)

	# Optionally, to make it compatible with the SceneTree.change_scene() API.
	get_tree().set_current_scene(current_scene)
	
	current_scene.show_error(error_msg)

func get_username(id):
	return get_dictionary_entry(id).username

func get_color(id):
	return get_dictionary_entry(id).color

func get_dictionary_entry(id):
	return Multiplayer.player_info.get(int(id))
