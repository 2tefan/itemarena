extends Node

enum ITEM_TYPES {NOTHING, NORMAL}
var sprites = ["PlayerItem", "NormalItem"]

remotesync func remove_item_from_map(mapPath, itemPath):
	var item = get_node(itemPath)
	var map = get_node(mapPath)
	item.get_parent().remove_child(item)
	map.hidden_items.add_child(item)
	item.set_angular_velocity(0) # Removes angluar velocity
	item.set_linear_velocity(Vector2(0,0)) # Removes velocity
	item.rotation = 0
	item.disable_item()

remotesync func add_item_to_map(mapPath, itemPath, position, linear_velocity):
	var item = get_node(itemPath)
	var map = get_node(mapPath)
	item.position = position
	item.set_linear_velocity(linear_velocity)
	item.get_parent().remove_child(item)
	map.normal_items.add_child(item) # Adds the item back to the game
	item.enable_item()
	item.move_event()
