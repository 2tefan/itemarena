extends Node

var root = null
export var path_to_game_results = "res://GUI/Menu/GameResults/GameResults.tscn"
export var path_to_map_loading_screen= "res://GUI/Menu/MapLoadingScreen/MapLoadingScreen.tscn"

func _ready():
	root = get_tree().get_root()

func _get_current_scene():
	return root.get_child(root.get_child_count() - 1)

func load_game_results(results):
	call_deferred("_deferred_load_game_results", results) # Only load new scene after everything else is finish (or problems may occur)

func _deferred_load_game_results(results):
	var current_scene = _get_current_scene()
	current_scene.call_deferred("free")
	
	var new_scene = ResourceLoader.load(path_to_game_results)
	var new_scene_instance = new_scene.instance()
	root.add_child(new_scene_instance)

	# Optionally, to make it compatible with the SceneTree.change_scene() API.
	get_tree().set_current_scene(new_scene_instance)
	new_scene_instance.set_results(results)

func load_map(path_to_map):
	Global.current_map = path_to_map
	get_tree().change_scene(path_to_map_loading_screen)
