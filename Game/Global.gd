extends Node

var main_menu = "GUI/Menu/StartMenu/StartMenu.tscn"
var current_map = null
var debug_mode = false

func _ready():
	OS.set_window_title("ItemArena [" + String(Version.game_version) + "]")
	
	for argument in OS.get_cmdline_args():
		if argument == "-FRANZI_CLIENT":
			Network.is_server = false
			Network.set_server_ips(["127.0.0.1"])
			Network.port = 2020
			get_tree().change_scene("GUI/Menu/MultiplayerLoadingScreen/MultiplayerLoadingScreen.tscn")
			print("FRANZI_CLIENT")
		
		if argument == "-DEBUG":
			debug_mode = true
