extends Node

#warnings-disable: 
var port = 2020
var is_server = true
var server_ips = ["127.0.0.1"]
var formatted_server_ips = []

export var public_ip_lookup = false

var players = 4

signal reload_gui

func server_set_ips(with_public_ip = false):
	set_server_ips(get_ips(with_public_ip))

func set_server_ips(new_server_ips):
	server_ips = new_server_ips
	formatted_server_ips = PoolStringArray(server_ips).join("\n")

func get_ips(with_public_ip):
	
	if with_public_ip:
		request_public_ip()
		return get_local_ips(["Waiting for public ip..."])
	
	return get_local_ips()

func get_local_ips(ips = []):
	var interfaces = IP.get_local_interfaces()
	
	for interface in interfaces:
		for address in interface.addresses:
			if address.is_valid_ip_address():
				if _is_ipv6(address):
					if _usable_ipv6(address):
						ips.append(_shorten_ipv6(address))
				else:
					if _usable_ipv4(address):
						ips.append(address)
	
	return ips

func request_public_ip():
	var httpr = HTTPRequest.new()
	add_child(httpr)
	httpr.connect("request_completed", self, "_on_request_completed")
	
	var error = httpr.request("https://api.ipify.org")
	
	if error != OK:
		push_error("An error occurred in the HTTP request.")
	
	
	var ip = yield() # Waits until response from server
	httpr.disconnect("request_completed", self, "_on_request_completed")
	httpr.free()
	
	if ip != null:
		set_server_ips(get_local_ips([ip]))
	else:
		set_server_ips(get_local_ips())
	
	emit_signal("reload_gui")

func _on_request_completed(_result, response_code, _headers, body):
	var output = null
	if response_code == 200:
		output = body.get_string_from_utf8()
	
	request_public_ip().resume(String(output))

func _is_ipv6(ip):
	return ip.find(":") != -1

func _shorten_ipv6(ip):
	var start_redundant_stuff = ip.find(":0:", 0)
	
	if start_redundant_stuff != -1:
		
		var end_redundant_stuff = ip.find(":0:", start_redundant_stuff+1)
		
		while ip.find(":0:", end_redundant_stuff+1) == end_redundant_stuff+2:
			end_redundant_stuff = ip.find(":0:", end_redundant_stuff+1)
		
		var new_ip = ip.substr(0, start_redundant_stuff) + "::" + ip.substr(end_redundant_stuff+3, ip.length())
		
		return new_ip

func _usable_ipv6(ip) -> bool:
	return !_is_link_local_v6(ip)
	
func _usable_ipv4(ip) -> bool:
	return !_is_link_local_v4(ip) and !_is_apipa(ip)


func _is_link_local_v6(ip) -> bool:
	return ip == "0:0:0:0:0:0:0:1"

func _is_link_local_v4(ip) -> bool:
	return ip == "127.0.0.1"

func _is_apipa(ip) -> bool:
	return !ip.findn("169.254.", 0)
