#!/bin/bash

PROJECT_ROOT="$(pwd)"
GIT_VERSION="$(git describe)"

sed -i 's/local_development/'$GIT_VERSION'/g' Game/Version.gd

for arg in "$@"; do
    case "${arg}" in
    --linux | -l)
        LINUX=YES
        ;;
    --windows | -w)
        WINDOWS=YES
        ;;
    --macos | -m)
        MACOS=YES
        ;;
    esac
done

function godotBuild () {
    cd $PROJECT_ROOT
    mkdir -v -p "$PROJECT_ROOT/$GIT_VERSION/$2"
    godot -v --export "$1" "$PROJECT_ROOT/$GIT_VERSION/$2/$3"
}

if [ "$LINUX" == "YES" ]; then
    godotBuild "Linux/X11" "linux" "itemarena.x86_64"
fi

if [ "$WINDOWS" == "YES" ]; then
    godotBuild "Windows Desktop" "windows" "itemarena.exe"
fi

if [ "$MACOS" == "YES" ]; then
    godotBuild "Mac OSX" "macos" "itemarena.zip"
fi

sed -i 's/'$GIT_VERSION'/local_development/g' Game/Version.gd